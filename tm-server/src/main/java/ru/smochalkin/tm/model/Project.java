package ru.smochalkin.tm.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@Table(name = "tm_project")
public final class Project extends AbstractBusinessEntity {

    @Nullable
    @OneToMany(mappedBy = "project")
    private List<Task> tasks = new ArrayList<>();

}